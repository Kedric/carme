/**
 * Written by Kedric Salisbury
 */
"use strict";
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');

const create = require('./routes/create');
const users = require('./routes/users');
const mongoose = require('mongoose');
const fs = require('fs');
const configDB = require('./config/database');
const uuidv4 = require('uuid/v4');
/**
 * All of these are libraries written by various people to help accomplish things such as
 * Database management, uuids, sessions, etc...
 * However the rest of the code written is a utilization of these NodeJS libraries.
 */

mongoose.connect(configDB.url);

const app = express();
const router = express.Router();
const http = require('http').Server(app);
const port = 82;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(session({ secret: 'special session for auth ya this is my secret text', cookie: {}}));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
http.listen(port);
http.listen(port, function() {
    console.log('Server listening on port ' + port);
});
http.on('error', (e) => {
    console.log(e);
});
/***
 * ABSTRACTION
 */
let Car;
let carItemSchema;
carItemSchema = mongoose.Schema({
    id: String,
    carSize: {type: Number, default: 5},
    driver: {type: String},
    seats: [String],
    date: {type: Date},
    seatsLeft : {type: Number}
});
carItemSchema.static('findByDriver', function (name, callback) {
    return this.findOne({ 'driver': name }, callback);
});
carItemSchema.static('findByID', function (id, callback) {
    return this.findOne({'id' : id}, callback);
});
Car = mongoose.model('Car', carItemSchema);
/**
 * END OF ABSTRACTION
 */

router.get('/', function(req, res, next){
    console.log("calledddddd");
    console.log(req.session.lastID);
    res.render("create", {lastID: req.session.lastID});
});

router.get("/create", function(req, res, next){
    console.log("called");
    res.render("create", {lastID: req.session.lastID});
});

router.post("/create", function(req, res, next){
  let driver = req.body.driver;
  let carSize = req.body.carSize;
  let time = Number(req.body.time);
  let id = uuidv4();
  let seats = [driver];
  let seatsLeft = carSize-seats.length;
  let newCar = new Car();
  newCar.driver = driver;
  newCar.carSize = carSize;
  newCar.id = id;
  newCar.seats = seats;
  newCar.seatsLeft = seatsLeft;
  newCar.date = new Date();
  newCar.save(function(err){
      req.session.lastID = id;
      if(err){
          next(err);
      }
      res.render("create", {lastID: req.session.lastID});
  });
  // hours
    setTimeout(() => {
        Car.remove({id: id}, function (err) {
        });
    }, 1000*60*60*time);//Algorithm (There are multiple, but I chose this one)
});
router.post("/claimSeatID", function(req, res, next){
    let id = req.body.id;
    Car.findByID(id, function(err, carFound){
       if(err){
           next(err);
           return;
       }
       carFound.seats.push(req.body.passenger);
       carFound.seatsLeft--;
       carFound.markModified('seatsLeft');
       carFound.markModified('seats');
       carFound.save(function (err) {
           if(err){
               next(err);
               return;
           }
           res.render("car",{id: carFound.id, date: carFound.date, driver: carFound.driver, carSize: carFound.carSize, seats: carFound.seats, seatsLeft: carFound.seatsLeft});
       });
    });
});
router.post("/remove", function(req, res, next) {
    if (req.session.lastID === req.body.id) {
        let id = req.body.id;
        Car.findByID(id, function (err, carFound) {
            if (err) {
                next(err);
                return;
            }
            var index = carFound.seats.indexOf(req.body.passenger);
            if (index > -1) {
                carFound.seats.splice(index, 1);
            }
            carFound.seatsLeft++;
            carFound.markModified('seatsLeft');
            carFound.markModified('seats');
            carFound.save(function (err) {
                if (err) {
                    next(err);
                    return;
                }
                res.render("car", {
                    id: carFound.id,
                    date: carFound.date,
                    driver: carFound.driver,
                    carSize: carFound.carSize,
                    seats: carFound.seats,
                    seatsLeft: carFound.seatsLeft
                });
            });
        });
    }
});
router.get("/car/:id", function (req, res, next) {
    if (req.params.id !== null && req.params.id !== undefined) {

        let id = req.params.id;
        Car.findByID(id, function (err, carFound) {
            if (err) {
                res.render("create", {lastID: req.session.lastID});
                return;
            }
            if(carFound === null){
                res.render("create", {lastID: req.session.lastID});
                return;
            }
            res.render("car", {
                id: carFound.id,
                date: carFound.date,
                driver: carFound.driver,
                carSize: carFound.carSize,
                seats: carFound.seats,
                seatsLeft: carFound.seatsLeft
            });
        });
    }else{
        res.render("create", {lastID: req.session.lastID});
    }
});
app.use('/', router);

module.exports = app;
